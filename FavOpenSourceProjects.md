# Following Open Source Projects 

| Software | Purpose |
|----------|---------|
| [Apache](https://www.apache.org/) | Web Server |
| [Open LDAP](https://www.openldap.org/) | Lightweight Directory Access |
| [Open SSL](https://www.openssl.org) | Secure Sockets Layer |
| [Perl](https://www.perl.org/) | Programming Language |
| [GCC](https://gcc.gnu.org/) | GNU C Compiler |
| [Tcl/Tk](https://www.tcl.tk/) | Tool Command Language |
| [Open SS7](http://www.openss7.org/)[^1] | Telephone network and next generation network Signalling System No. 7 (SS7)and Signalling Transport (SIGTRAN) protocol stack |

[^1]: Dead project
